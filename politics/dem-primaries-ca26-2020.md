# Democratic Primaries for March 3, 2020

# CITY/LOCAL (for CA29 area)

> (You can see the map of the area here - https://www.govtrack.us/congress/members/CA/29 - it's NoHo, Van Nuys, Sun Valley, Lakeview Terrace, and Sylmar)

---

## Los Angeles City Primary Nominating Election
### *Member of the City Council, 2nd District*

I picked Ayinde Jones because he has a track record of being a mental health professional for kids and such, and just because it seems like the incumbent Paul Krekorian is a little too friendly with the cops, but he's not the worst - Rudy Melendez is a fucking republican who had baby yoda face on his instagram when I looked him up so fuck him

---

## Los Angeles Unified School District
### *Member of the Board of Education, District 3*

I picked the incumbent Scott Mark Schmerelson, which is into smaller class sizes and not funding more charter schools until the existing public schools are cleaned up, which I am into, also 'Repairs Over iPads'

https://ballotpedia.org/Scott_Mark_Schmerelson

---

## Member of the State Assembly
### *46th District*

The incumbent Adrin Nazarian seems decent enough, I could find zero information on his opponent online outside of her name, so incumbent it is.

https://a46.asmdc.org/

---

## United States Representative
### *29th District*

The incumbent seems like just a boring liberal democrat towing the line, while there are some way more interesting and progressive candidates running. Michael R Guzik has a HILARIOUS geocities-ass website that is worth seeing ( https://www.mrg2020.com/ ) but for my money, Angelica Duenas is 100% the pick, she's on board with the medicare 4 all/education 4 all/reducing the power of the criminal justice system that i'm into

https://www.angelica4congress.com/

---

# COUNTY (Los Angeles County)

## District Attorney

> A guide if you want to read some on this yourself: https://laist.com/2020/02/04/los_angeles_district_attorney_race_da_candidates_voter_guide.php

They're in charge of all the prosecutors for LA county, so I'd argue this is pretty fucking important - the incumbent Jackie Lacey is the first woman and first black person to be voted in, but she's also pro-death penalty, and didn't prosecute two cops for shooting and killing an unarmed mentally ill black man.

Of the two other candidates, Rachel Rossi and George Garcon, they have a lot of similar positions, but George is a little more hesitant to propose some of the more radical solutions for change that Rachel does, such as the decriminalization of homelessness (having it be illegal to be homeless is insane, and she rightfully recognizes that the homelessness problem needs to be addressed). Rachel is also the only one to suggest using outside prosecutors to investigate police shootings, which over the past few years has, uh, been PROBLEMATIC.

Rachel is definitely my pick.

https://rachel4da.com/

---

>> **HONESTLY YOU CAN PROBABLY JUST PICK JUDGES AT RANDOM BUT IM DOING THE RESEARCH BECAUSE I HATE MYSELF**

Before we get into the judges section it's worth mentioning how shit the process of picking judges is. Most of the time you can sort of pick them at random and be okay, because the system is fundamentally broken, as this ProPublica article states. It's all about endorsements and who you know.

Also, these are all top tier bootlickers. They're working to prosecute people in the LA criminal justice system, so they sort of all are. This is definitely a "least bad" in a lot of cases. I'm going to skew strongly against candidates endorsed by police organizations (since, y'know, they want convictions).

https://www.propublica.org/article/we-investigated-magistrates-now-lawmakers-want-to-overhaul-the-system

---

## Judge of the Superior Court
### Office 17

Woke white liberal lady works in Hardcore Gang unit, runs unopposed. Oh well.

https://www.cooleyforjudge.com/

### Office 42

Robert Vila was in the 'Hardcore Gang' unit, which worked on prosecuting gang murders, and got an endorsement from Jackie Lacey, the one who defended the cops for killing the unarmed mentall ill black man. Least bad seems like Linda Sun.

https://lindasunforjudge.com/

### Office 72

Of the three options, it seems like Myana Dellinger is probably the least bad here. She leans heavily on her platforms, she's the only website that is available in three langues for her district (english, spanish, korean), and also the only one that has any kind of union representation. One of the dudes seems kind of like an ex-military chud, the other guy seems nice but his website doesn't give enough information about his platform, though I get vaguely conservative vibes. Myana seems like the pick.

https://www.dellingerforjudge.com/

### Office 76

This one is kind of hilarious - a retired counselor named Michael Richard Cummins legally changed his name to Judge Mike Cummins, so it shows up on the ballot as Judge Mike Cummins, but he's not a judge. There was a law passed where you have to use your legal government job title, so that was his answer.

Anyways - his opponent Emily seems decent.

https://www.emilycoleforjudge2020.com/

### Office 80

Three dudes that again seem to be relative carbon copies of one another. One had endorsements from a bunch of cops, though, so not that guy. Seems like Klint McKay has a decent track record, though.

http://klintmckayforjudge.com/

### ~~Microsoft~~ Office 97

Two people, one with a bunch of cop endorsements. That one made it easy for me. Tim Reuben seems decent.

https://www.reubenforjudge.com/

### Office 129

Mark MacCarley wanted to have the title of 'retired army general' on the ballot, but he has to be 'lawyer' and that's hilarious. Also, he's a Republican, so fuck him anyways. He and the other guy both have a bunch of cop endorsements.

Least bad is Bruce A Moss.

https://mossforjudge.com/

### Office 131

The only lady running uncontested has the most white girl name I've ever heard - Kelly Michelle Kelley. I don't see any endorsements and she seems decent, so, uh, you don't really have a choice either way.

https://michellekelleyforjudge.com/

### Office 141

Uncontested run by Lana Kim. She seems decent, though. No cop endorsements.

https://www.lanakimforjudge.com/

### Office 145

Two candidates here - Adan Montalban has giant cop endorsements in his fucking banner image. The other one, Troy Slaten, was on episodes of Parker Lewis Can't Lose, a TV show my mom worked on in the early 90s. He at least seems like less of a bootlicker.

https://troyslatenforjudge.com/

### Office 150

At this point in time in the process, I can look at all the candidates in the category and go "which one most looks like a bootlicker" - and the one I pick almost always has a bunch of cop endorsements. Manuel Almada has a bunch of cop endorsements, Tom Parsekian has an Adam Schiff endorsement (which I'd rate as a negative but ymmv) and Onica Valle Cole seems like a nice mom? I dunno. Tom seems the most likely to win, even though #NiceMom2020 I'm into.

https://parsekianforjudge.com/

### Office 162

David Diamond is endorsed by the legal organization that protects cops. So, y'know. Scott Andrew Yang (does he give out 1000 Freedom Get Out Of Jail Free cards for Americans?) has endorsements from major cop organizations. Caree Annett Harper is an ex-cop who left the force after saving someone's life and now focuses on civil rights issues, so she actually seems kinda cool.

https://www.harper4judge.com/

---

>> **YOU HAVE SURVIVED THE JUDGE ZONE, WELL DONE**

---

## Member, County Central Committee, 46th Assembly District

Okay, so this one is weird. It is apparently a known fact that getting information on these people is [fucking impossible](https://patch.com/california/westhollywood/bp--blog-what-is-the-county-central-committee-that-ape3d065d617). You can literally go to [Voter's Edge](https://votersedge.org) and plug in a zip code and get... [not a lot of info](https://votersedge.org/ca/en/ballot/election/83-5c68d6/address/null/zip/91601/contests/contest/19959?&date=2020-03-03).

What is being voted on, basically, is the governing body for the party in the county. So I'll be honest, on this one we're sort of rolling the dice. As we've seen in Iowa at the start of this 2020 Democratic primary process, I am not personally at a high level of confidence in the Democratic party as a whole, so... ymmv.

I'm going to mention when the [North Valley Democratic Club (NVDC)](http://nvdemclub.org/) endorses them, because that's literally the only endorsement metric I could find. If you're honestly curious, [this website](https://lavote.net/Apps/CandidateList/Index?id=4085) has their email addresses/phone numbers, so uh... ask them yourself I guess?

* John Bryan Alford - **Probably?** Is on the executive board of the San Fernando Valley Democratic Party. NVDC endorsed.
* Alton Reed - **Probably?** Democratic incumbent. 46th AD Delegation Chair of the LA County Democratic Party.
* Joyce Rubin - **Maybe?** All I can find is that she was on the committee before.
* David Alan Hyman - **Maybe?** Same as above, he was on the committee before. NVDC endorsed.
* Carolyn Hoff - **Probably?** NVDC endorsed.
* Dan Lopez - _**NO.**_ Worked for Kamala Harris the Cop.
* Brandon (Don E Ford) Nelson - **No Info!** I have no idea why he has an entirely separate name in his parenthetical.
* Paul Kujawsky - _**NO.**_ The only information I could find on him is some archived [SmartVoter page](http://www.smartvoter.org/2012/06/05/ca/la/vote/kujawsky_p/) where he says 'I will oppose extremism in the Democratic Party', so, y'know, fuck that guy and his pacbell.net email address.
* Jose Sandoval - **Maybe?** His title is 'Animal Rights Activist', so that's cool I guess.
* Jaye "Bonnie" Shatun - **Maybe?** She was on the committee before.
* Jerilyn V. Stapleton - **Probably?** She's on the [Stonewall Democratic Club](https://www.stonewalldems.org/) Steering Commitee, and is apparently an incumbent. Probably a safe bet.

So, if we cut out the two hard NOs, that leaves us with 9 candidates. Drop two of the maybes arbitrarily and you'll be fine.

## County Measure R

More oversight into cops. Easy yes.

# STATE

## Measure 13

Fund a slight tax increase to help make public schools better. Don't fall into the charter school exceptionalism narrative.

# NATIONAL ELECTION

## Presidential Preference

>> BERNARD

>> BIG DICK

>> SANDERS

# IN CLOSING
None of this matters vote bernie